package com.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dto.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	@Query("from User u where u.username = :username")
	public User getUserByName(@Param("username") String username);

	@Query("from User u where u.email = :email and u.password = :password")
	public User login(@Param("email") String email, @Param("password") String password);
	
}