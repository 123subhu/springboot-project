package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.dto.User;

@Service
public class UserDAO {

	@Autowired
	UserRepository userRepo;

	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	public User registerUser(User user) {
		return userRepo.save(user);
	}

	public User getUserById(int userId) {
		User user = new User(0,"",0,"","");
		return userRepo.findById(userId).orElse(user);
	}

	public User getUserByName(String username) {
		User user = new User(0,"",0,"","");
		User useR = userRepo.getUserByName(username); 
		
		if(useR!=null){
			return useR;
		}
		
		return user;
	}

	public void removeUser(int userid) {
		userRepo.deleteById(userid);
	}

	public User login(String email, String password) {
		return userRepo.login(email, password);
	}
	
	
	
}