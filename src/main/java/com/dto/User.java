package com.dto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User {
	@Id@GeneratedValue
	private int userId;

	private String username;
	
	private long phoneNum;

	private String email;
	
	private String password;

	public User() {
		super();
	}

	
	public User(int userId, String username, long phoneNum, String email, String password) {
		super();
		this.userId = userId;
		this.username = username;
		this.phoneNum = phoneNum;
		this.email = email;
		this.password = password;
	}

	
	public long getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(long phoneNum) {
		this.phoneNum = phoneNum;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
	    return "User [userId=" + userId + ", username=" + username + ", phoneNum=" + phoneNum + ", email=" + email
	            + ", password=" + password + "]";
	}


	
}