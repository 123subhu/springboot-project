package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.UserDAO;
import com.dto.User;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

	@Autowired
	UserDAO userDAO;
	
	@GetMapping("/getAllUsers")
	public List<User> getAllUsers(){
		return userDAO.getAllUsers();
	}
	
	@PostMapping("/registerUser")
	public User registerUser(@RequestBody User user){
		return userDAO.registerUser(user);
	}
	
	@PutMapping("/updateUser")
	public User updateUser(@RequestBody User user){
		return userDAO.registerUser(user);
	}
	
	@GetMapping("/getUserById/{id}")
	public User getUserById(@PathVariable("id")int userId){
		return userDAO.getUserById(userId);
	}
	
	@GetMapping("/getUserByName/{name}")
	public User getUserByName(@PathVariable("name") String username){
		return userDAO.getUserByName(username);
	}
	
	@DeleteMapping("/removeUser/{id}")
	public String removeUser(@PathVariable("id")int userid){
		userDAO.removeUser(userid);
		return "User removed!!!";
	}
	
	@GetMapping("/login/{email}/{password}")
	public User login(@PathVariable("email")String email,@PathVariable("password")String password){
		return userDAO.login(email,password);
	}
}